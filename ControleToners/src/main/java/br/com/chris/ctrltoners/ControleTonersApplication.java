package br.com.chris.ctrltoners;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControleTonersApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControleTonersApplication.class, args);
	}
}

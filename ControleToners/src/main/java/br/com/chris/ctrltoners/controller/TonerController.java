package br.com.chris.ctrltoners.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sun.mail.handlers.handler_base;

import br.com.chris.ctrltoners.model.Toner;
import br.com.chris.ctrltoners.model.Toners;

@Controller
public class TonerController {
	
	@Autowired
	private Toners toners;
	
	@RequestMapping("/toners")
	public String lista(Model model) {
		model.addAttribute("toners", toners.findAll());
		return "toner/lista";
	}
	
	@GetMapping("/toner")
	public String form(Long id, Model model) {
		if (id == null)
			model.addAttribute("toner", new Toner());
		else
			model.addAttribute("toner", toners.findOne(id));
		return "toner/form";
	}
	
	@PostMapping("/toner")
	public String salva(@Valid Toner toner, BindingResult result) {
		if (descricaoJaExiste(toner))
			result.addError(new FieldError("toner", "descricao", "Descrição já cadastrada"));
		if (result.hasErrors())
			return "toner/form";
		toners.save(toner);
		return "redirect:toners";
	}

	@RequestMapping("/aumentaToner")
	public String aumenta(Long id, Model model, HttpServletResponse response) {
		Toner toner = toners.findOne(id);
		toner.setQuantidade(toner.getQuantidade() + 1);
		toners.save(toner);
		model.addAttribute("retorno", toner.getQuantidade());
		response.setStatus(200);
		return "retorno";
	}
	
	@RequestMapping("/diminuiToner")
	public String diminui(Long id, Model model, HttpServletResponse response) {
		Toner toner = toners.findOne(id);
		if (toner.getQuantidade() == 0) {
			response.setStatus(500);
			return "retorno";
		}
		toner.setQuantidade(toner.getQuantidade() - 1);
		toners.save(toner);
		model.addAttribute("retorno", toner.getQuantidade());
		response.setStatus(200);
		return "retorno";
	}
	
	@RequestMapping("/removeToner")
	public void deleta(Long id, HttpServletResponse response) {
		toners.delete(id);
		response.setStatus(200);
	}
	
	
	// private ======================================================
	private boolean descricaoJaExiste(Toner toner) {
		Toner tonerDb = toners.findByDescricao(toner.getDescricao());
		if (tonerDb == null)
			return false;
		return toner.getId() != tonerDb.getId();
	}
}

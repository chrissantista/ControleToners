package br.com.chris.ctrltoners.model;


import org.springframework.data.jpa.repository.JpaRepository;

public interface Toners extends JpaRepository<Toner, Long> {
	Toner findByDescricao(String descricao);
}
